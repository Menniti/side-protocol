package rest

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"side-protocol.com/core"
	_error "side-protocol.com/utils/error"
)

type HandlerMovie struct {
	coreMovie core.IMovie
}

//Build new movie struct
func BuildHandlerMovie(coreMovie core.IMovie) HandlerMovie {
	return HandlerMovie{coreMovie: coreMovie}
}

func (h HandlerMovie) SearchMovie(c *gin.Context) {
	searchText := c.Query("search_text")
	page := c.Query("page")
	if page == "" {
		page = "1"
	}
	log.Println(searchText)
	log.Println(page)
	if searchText == "" {
		c.JSON(http.StatusBadRequest, Response{Status: http.StatusBadRequest, Message: _error.HandlerParamsSearchTextError.Error()})
		return
	}
	search, err := h.coreMovie.SearchMovie(searchText, page)
	if err != nil {
		c.JSON(http.StatusNotFound, Response{Status: http.StatusNotFound, Message: _error.HandlerSearchResponseError.Error()})
		return
	}

	if len(search.Search) == 0 {
		c.JSON(http.StatusNotFound, Response{Status: http.StatusNotFound, Message: _error.NotFound.Error()})
		return
	}

	c.JSON(http.StatusOK, Response{Status: http.StatusOK, Message: "OK", Data: search})
	return

}

func (h HandlerMovie) GetMovieByID(c *gin.Context) {
	id := c.Params.ByName("id")

	if id == "" {
		c.JSON(http.StatusBadRequest, Response{Status: http.StatusBadRequest, Message: _error.HandlerParamsMovieIDError.Error()})
		return
	}

	movieDetails, err := h.coreMovie.GetMovieByID(id)
	if err != nil {
		c.JSON(http.StatusNotFound, Response{Status: http.StatusNotFound, Message: _error.HandlerMovieDetailsResponseError.Error()})
		return
	}

	if movieDetails.Title == "" && movieDetails.Year == "" {
		c.JSON(http.StatusNotFound, Response{Status: http.StatusNotFound, Message: _error.NotFound.Error()})
		return
	}

	c.JSON(http.StatusOK, Response{Status: http.StatusOK, Message: "OK", Data: movieDetails})
	return
}

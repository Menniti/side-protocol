package core

import (
	"encoding/json"
	"fmt"
	"log"

	"side-protocol.com/config"
	"side-protocol.com/infra/requests"
	_error "side-protocol.com/utils/error"
)

type IMovie interface {
	SearchMovie(searchText string, page string) (Search, error)
	GetMovieByID(id string) (MovieDetails, error)
}

func BuildMovie() IMovie {
	return Movie{}
}

func (m Movie) SearchMovie(searchText string, page string) (Search, error) {
	search := Search{}
	requestURL := fmt.Sprintf("%s?apikey=%s&s=%s&p=%s", config.URL, config.KEY, searchText, page)
	bytes, err := requests.GetRequest(requestURL)
	if err != nil {
		log.Println(err)
		log.Println(_error.HttpRequestError)
		return search, err
	}

	err = json.Unmarshal(bytes, &search)
	if err != nil {
		log.Println(err)
		log.Println(_error.UnmarshalError)
		return search, err
	}

	return search, nil
}

func (m Movie) GetMovieByID(id string) (MovieDetails, error) {
	movieDetails := MovieDetails{}
	requestURL := fmt.Sprintf("%s?apikey=%s&i=%s", config.URL, config.KEY, id)
	bytes, err := requests.GetRequest(requestURL)
	if err != nil {
		log.Println(_error.HttpRequestError)
		return movieDetails, err
	}
	err = json.Unmarshal(bytes, &movieDetails)
	if err != nil {
		log.Println(_error.UnmarshalError)
		log.Println(err)
		return movieDetails, err
	}

	return movieDetails, nil
}

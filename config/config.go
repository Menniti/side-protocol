package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var KEY string
var URL string
var HTTP_TIMEOUT string
var HTTP_PORT string

//LoadConfig - LoadConfig from .env file
func LoadConfig(path string) {
	err := godotenv.Load(path)
	if err != nil {
		log.Fatal("Error loading .env file")
		return
	}
	KEY = os.Getenv("KEY")
	URL = os.Getenv("URL")
	HTTP_TIMEOUT = os.Getenv("HTTP_TIMEOUT")
	HTTP_PORT = os.Getenv("HTTP_PORT")
}

package rest

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type Router struct {
	hMovie HandlerMovie
}

func BuildRouter(hMovie HandlerMovie) Router {
	return Router{hMovie: hMovie}
}

//SetupRouter - URL router of REST Protocol
func (router *Router) SetupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	r.Use(TimeOutMiddleware())

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	r.GET("/healthcheck", func(c *gin.Context) {
		c.String(http.StatusOK, "healthcheck")
	})

	r.GET("/movie/search", router.hMovie.SearchMovie)
	r.GET("/movie/detail/:id", router.hMovie.GetMovieByID)

	return r
}

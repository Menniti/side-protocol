package main

import (
	"fmt"

	"side-protocol.com/config"
	"side-protocol.com/core"
	"side-protocol.com/infra/rest"
)

func main() {

	config.LoadConfig("./config/.env")

	movie := core.BuildMovie()

	handlerMovie := rest.BuildHandlerMovie(movie)

	router := rest.BuildRouter(handlerMovie)

	r := router.SetupRouter()

	r.Run(fmt.Sprintf(":%s", config.HTTP_PORT))
}

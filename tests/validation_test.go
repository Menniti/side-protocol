package tests

import (
	"testing"

	"side-protocol.com/utils/validations"
)

func TestValidateStrToInt(t *testing.T) {
	expectedBool := true
	expectedInt := 10
	validInt, isValidInt := validations.ValidateStrToInt("10")
	if isValidInt != expectedBool {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedBool, isValidInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedBool, isValidInt)
	}

	if expectedInt != validInt {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedInt, validInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedInt, validInt)
	}

	expectedBool = false
	expectedInt = 0
	validInt, isValidInt = validations.ValidateStrToInt("a")
	if isValidInt != expectedBool {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedBool, isValidInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedBool, isValidInt)
	}

	if expectedInt != validInt {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectedInt, validInt)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectedInt, validInt)
	}
}

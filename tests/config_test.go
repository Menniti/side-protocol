package tests

import (
	"os"
	"testing"

	"side-protocol.com/config"
)

func TestConfig(t *testing.T) {

	config.LoadConfig("../config/.env")
	expectHTTP_TIMEOUT := "5"
	HTTP_TIMEOUT := os.Getenv("HTTP_TIMEOUT")
	if expectHTTP_TIMEOUT != HTTP_TIMEOUT {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectHTTP_TIMEOUT, HTTP_TIMEOUT)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectHTTP_TIMEOUT, HTTP_TIMEOUT)
	}

	expectHTTP_PORT := "9090"
	HTTP_PORT := os.Getenv("HTTP_PORT")
	if expectHTTP_PORT != HTTP_PORT {
		t.Errorf("FAILED, expected -> %v, got -> %v", expectHTTP_PORT, HTTP_PORT)
	} else {
		t.Logf("SUCCEDED, expected -> %v, got -> %v", expectHTTP_PORT, HTTP_PORT)
	}

}

package error

import "errors"

var HttpRequestError = errors.New("Client HttpRequest Error")
var UnmarshalError = errors.New("Error to unmarshal bytes to struct")

var HandlerParamsSearchTextError = errors.New("Please provide a search_text parameter")
var HandlerParamsMovieIDError = errors.New("Please provide a id movie parameter")

var HandlerSearchResponseError = errors.New("Error to get search, please review the parameters")
var HandlerMovieDetailsResponseError = errors.New("Error to get movie details, please review the parameters")

var NotFound = errors.New("Not Found")

var RequestClientError = errors.New("Error to get search, please review the parameters")

package validations

import "strconv"

//Validate Str to Int
func ValidateStrToInt(stringToInt string) (integer int, boolean bool) {
	sizeInt, err := strconv.Atoi(stringToInt)
	if err != nil {
		return 0, false
	}
	return sizeInt, true

}
